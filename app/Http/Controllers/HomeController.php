<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\maxpoint\Clasificacion;
use App\Entities\maxpoint\MenuAgrupacion;
use App\Entities\Maxpoint\ProductosMenusKiosko;
use App\Entities\Maxpoint\Menu;
use Illuminate\Support\Collection;
use App\Entities\Maxpoint\Plus;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function mag(Request $request)
    {
        $plus=Plus::with("preguntas")
            ->where("plu_id",$request->get("plu_id"))
            ->firstOrFail();
        dd($plus);
        return $plus;
    }
}
