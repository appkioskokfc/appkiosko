<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Kiosko\KioskoEstaciones;
use App\Entities\maxpoint\Estacion;
use App\Entities\maxpoint\UsersPos;
use App\Http\Controllers\Controller;
use App\Serializers\SelectsSerializer;
use App\Transformers\Admin\EstacionKioskoTransformer;
use App\Transformers\Admin\EstacionSelectTransformer;
use App\Transformers\Admin\UsersPosSelectTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class EstacionesController extends Controller
{
    public function listar(Request $request)
    {
        if ('POST' === $request->method()) {
            $resultado = new \stdClass();
            $resultado->Result = "OK";
            $resultado->Records = KioskoEstaciones::all();

            return json_encode($resultado);
        }
        return view('admin.estaciones.listar'); //->withPosts($posts);
    }

    public function crear(Request $request)
    {

        $inputUsuario=$request->usuario_pos;
        $usuarioPos=UsersPos::where("usr_nombre_en_pos",$inputUsuario)->first();
        $nuevaEstacion = new KioskoEstaciones();
        $nuevaEstacion->ip = $request->ip;
        $nuevaEstacion->clave_pos = $usuarioPos->urs_varchar1;
        $nuevaEstacion->usuario_pos = $usuarioPos->usr_nombre_en_pos;
        $estadoCreacion = $nuevaEstacion->save();

        $resultado = new \stdClass();
        $resultado->Result = "OK";
        $resultado->Record = $nuevaEstacion;
        return json_encode($resultado);
    }

    public function editar(Request $request)
    {
        $estacionActual = KioskoEstaciones::find($request->id);

        $inputUsuario=$request->usuario_pos;
        $usuarioPos=UsersPos::where("usr_nombre_en_pos",$inputUsuario)->first();

        $estacionActual->ip = $request->ip;
        $estacionActual->clave_pos = $usuarioPos->urs_varchar1;
        $estacionActual->usuario_pos = $usuarioPos->usr_nombre_en_pos;
        $estadoGuardado = $estacionActual->save();

        $resultado = new \stdClass();
        $resultado->Result = $estadoGuardado ? "OK" : "ERROR";
        return json_encode($resultado);
    }

    public function eliminar(Request $request)
    {
        $estacionActual = KioskoEstaciones::find($request->id);
        $estadoEliminacion = $estacionActual->delete();

        $resultado = new \stdClass();
        $resultado->Result = $estadoEliminacion ? "OK" : "ERROR";
        return json_encode($resultado);
    }

    public function comboEstacionesKioskoMaxpoint()
    {
        //Se utiliza esta expresión para filtrar las IPs correspondientes a kiosko,
        // que por convención se establecieron en el rango *.*.*.[30-39]. Ejemplo: 10.101.4.36
        $formatoIpsKiosko = '%.%.%.2%';

        $estaciones = Estacion::join('Status as e2', function ($join) {
            $join->on('Estacion.IDStatus', '=', 'e2.IDStatus')
                ->whereRaw("e2.std_descripcion='Activo' and mdl_id in(Select mdl_id from modulo where mdl_descripcion='Estaciones')");
        })->select(['est_ip'])
            ->where("est_ip", 'like', $formatoIpsKiosko)
            ->get();

        $fractal = new Manager();
        $data = new Collection($estaciones, new EstacionSelectTransformer());
        $jsonEstaciones = $fractal->setSerializer(new SelectsSerializer())->createData($data)->toJson();
        return $jsonEstaciones;
    }

    public function comboUsuariosKioskoMaxpoint()
    {
        $usuariosKioskoMaxpoint = UsersPos::join('Status as e2', function ($join) {
            $join->on('users_pos.IDStatus', '=', 'e2.IDStatus')
                ->whereRaw("e2.std_descripcion='Activo' and mdl_id in(Select mdl_id from modulo where mdl_descripcion='Seguridades')");
        })->select(['usr_nombre_en_pos','usr_descripcion'])->where("usr_nombre_en_pos", 'like', '%kiosko%')->get();

        $fractal = new Manager();
        $data = new Collection($usuariosKioskoMaxpoint, new UsersPosSelectTransformer());
        $jsonUsersPos = $fractal->setSerializer(new SelectsSerializer())->createData($data)->toJson();
        return $jsonUsersPos;
    }
}