<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Maxpoint\ProductosMenusKiosko;
use App\Entities\Maxpoint\MenuAgrupacion;
use App\Entities\Maxpoint\Menu;
use App\Http\Controllers\Controller;
use App\Serializers\ArraySerializer;
use App\Transformers\Admin\PlusKioskoTransformer;
use App\Transformers\Admin\CategoriasKioskoTransformer;
use File;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ImagenesController extends Controller
{
    public function listar(Request $request)
    {
        return view('admin.imagenes.listar');
    }

    public function listarPlus(Request $request)
    {
        $fractal = new Manager();
        $plus = ProductosMenusKiosko::all(["IDProducto", "Nombre"]);
        $data = new Collection($plus, new PlusKioskoTransformer);
        $jsonPlus = $fractal->setSerializer(new ArraySerializer())->parseExcludes('preguntas')->createData($data)->toJson();
        return $jsonPlus;
    }

    public function listarCategorias(Request $request)
    {
        $menuLocal=Menu::with(["menuAgrupaciones"=> function ($q) {
            $q->orderBy('mag_descripcion','asc');
        }])
        ->where("menu_Nombre","KIOSKO SALON")
        ->first();

        $fractal = new Manager();
        $data = new Collection($menuLocal->menuAgrupaciones, new CategoriasKioskoTransformer);
        $jsonCategorias = $fractal->setSerializer(new ArraySerializer())->parseExcludes('plus')->createData($data)->toJson();
        return $jsonCategorias;
    }

    public function guardarImagen(Request $request)
    {
        $fractal = new Manager();

        $idProducto = $request->get("idproducto");
        $tipoImagen = $request->get("tipoimagen");

        if("producto" === $tipoImagen){
            $producto = ProductosMenusKiosko::find($idProducto);
            $ubicacionImagen = $producto->ubicacionFoto;

            $nuevaFoto = $request->file("foto");
            if (File::exists($ubicacionImagen)) {
                File::delete($ubicacionImagen);
            }

            $nuevaFoto->move($producto->directorioAlmacenamiento, $producto->foto);
            $data = new Item($producto, new PlusKioskoTransformer);
            $jsonPlus = $fractal->setSerializer(new ArraySerializer())->createData($data)->toJson();
        }

        if("categoria" === $tipoImagen){
            $categoria = MenuAgrupacion::find($idProducto);
            $ubicacionImagen = $categoria->ubicacionFoto;
            //dd($ubicacionImagen);
            $nuevaFoto = $request->file("foto");
            if (File::exists($ubicacionImagen)) {
                File::delete($ubicacionImagen);
            }
            // dd($producto->getUbicacionFotoAttribute());
            $nuevaFoto->move($categoria->directorioAlmacenamiento, $categoria->foto);
            $data = new Item($categoria, new CategoriasKioskoTransformer());
            $jsonPlus = $fractal->setSerializer(new ArraySerializer())->createData($data)->toJson();
        }
        return $jsonPlus;
    }
}