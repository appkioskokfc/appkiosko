<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Entities\Maxpoint\TipoDocumento;
use Dingo\Api\Routing\Helpers;
use League\Fractal;
use League\Fractal\Manager;
use App\Transformers\TipoDocumentoTransformer;

class TipoDocumentoController extends Controller {

    use Helpers;

    public function listar() {
        $fractal = new Manager();
        $tipoDocumento = new TipoDocumento();
        $documentos = $tipoDocumento->join('Status as e2', function ($join) {
                    $join->on('Tipo_Documento.IDStatus', '=', 'e2.IDStatus')
                    ->where('e2.std_descripcion', '=', 'Activo');
                })->select(['Tipo_Documento.IDTipoDocumento', 'Tipo_Documento.tpdoc_descripcion'])
            ->where('Tipo_Documento.tpdoc_descripcion','<>','CONSUMIDOR FINAL')
                ->get();
        $data = new Fractal\Resource\Collection($documentos, new TipoDocumentoTransformer);
        $data->setMeta(["status" => "success"]);
        $resource = $fractal->createData($data)->toArray();
        return $resource;
    }

}
