<?php

namespace App\Http\Controllers\Api;

use GuzzleHttp\Exception\RequestException;
use Hamcrest\StringDescription;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Maxpoint\Cliente;
use App\Entities\Maxpoint\TipoDocumento;
use League\Fractal\Manager;
use App\Transformers\ClienteTransformer;
use GuzzleHttp\Client;
use DB;
use Illuminate\Support\Facades\Input;
use App\Utils\StringTools;

class ClienteController extends Controller
{
    public function buscar(Request $request,$documento)
    {
        $messages=[
            'IDTipoDocumento.required'=>'El campo es obligatorio (Tipo Documento).',
            'cli_nombres.required' => 'El campo es obligatorio (Nombre).',
            'cli_nombres.regex' => 'No se permiten números (Nombre).',
            'cli_nombres.max' => 'Se excedió la longitud máxima del campo (Nombre, :max caracteres).',
            'cli_nombres.min' => 'No se cumple la longitud minima del campo (Nombre, :min caracteres).',
            'max' => 'Se excedió la longitud máxima del campo (:attribute, :max caracteres).',
            'min' => 'No se cumple la longitud minima del campo (:attribute, :min caracteres).',
            'size' => 'Valor no válido (:attribute)',
            'cli_telefono.digits_between' =>'Se aceptan entre :min y :max dígitos (Teléfono)',
            'cli_telefono.required' => 'El campo es obligatorio (Teléfono).',
            'email' => 'No es un valor de email válido (Email)',
            'cli_documento.numeric' => 'El valor debe ser numérico (Documento)',
            'cli_documento.digits' => 'Se requieren exactamente :digits dígitos (Documento)',
            'cli_documento.regex' =>'No es un RUC válido (Documento)',
            'cedula' => 'No es una cédula válida'

        ];

        $validator = Validator::make($request->all(), [
            'tipodocumento' => 'required|min:36|max:40',
        ]);

        if ($validator->fails()) {
            return $this->responderConError("Se requiere el parametro tipodocumento (40 caracteres)");
        }

        $tipoDocumento=$request->get("tipodocumento");
        $nombreTipoDocumento=TipoDocumento::find($tipoDocumento,["tpdoc_descripcion"])->tpdoc_descripcion;

        if($nombreTipoDocumento==='CEDULA'){
            $validator = Validator::make(['cli_documento'=>$documento], [
                'cli_documento' => 'numeric | digits:10 | cedula',
            ],$messages);
            if ($validator->fails()) {
                return $this->responderConError("No es una cédula válida");
            }
            //$validator->validate();
        }
        if($nombreTipoDocumento==='RUC'){
            $validator = Validator::make(['cli_documento'=>$documento], [
                'cli_documento' => 'numeric | digits:13 | regex:/^\d{10}001$/u',
            ],$messages);
            if ($validator->fails()) {
                return $this->responderConError("No es un valor de RUC válido");
            }
        }

        // Buscar Cliente en base local
        $cliente = Cliente::where(
            'cli_documento', $documento
        )->where('IDTipoDocumento', $tipoDocumento)->first();
        if (!is_null($cliente)) {
            $cliente->cli_nombres= strtoupper(StringTools::normalize($cliente->cli_nombres));
            $cliente->cli_direccion=strtoupper(StringTools::normalize($cliente->cli_direccion));
            $cliente->cli_email=strtoupper($cliente->cli_email);
            //    Si el cliente existe se devuelve ese cliente
            return $cliente;
        }

        // Si no existe buscar cliente en azure
        // Encontrando la URL configurada del webService de clientes
        $sqlDireccionWebServices="exec [config].[USP_Retorna_Direccion_Webservice_restaurante] :nombreWebService,:nombreruta";
        $resultado = DB::selectOne(
            $sqlDireccionWebServices,
            [
                'nombreWebService' => 'SOA',
                'nombreruta'=>'CLIENTES BUSCAR'
            ]);
        $estado=(int)$resultado->estado;
        if(0===$estado){
            return $this->responderConError("Revisar configuración WS de clientes");
        }
        $urlWebServiceClientes=$resultado->direccionws.$documento.'&tipoIdentificacion='.$nombreTipoDocumento;
        $clienteHttp = new Client();
        try{
            $respuestaWS=$clienteHttp->request(
                'GET',
                $urlWebServiceClientes,
                [
                    'timeout' => 5,
                    'connect_timeout' => 5
                ]);
        }catch(RequestException $ex){
            //Aqui se podría disparar una alerta por correo electrónico avisando que el servicio web de clientes no está activo
            $respuesta=$this->responderConError("No existe el cliente");
            $respuesta->setStatusCode(404);
            return $respuesta;
        }

        $strRespuesta=(String)$respuestaWS->getBody();
        $datosConsultaClienteObj=json_decode($strRespuesta);
        $estadoWSClientes=$datosConsultaClienteObj->estado;
        if(1!==$estadoWSClientes){
            $respuesta=$this->responderConError("No existe el cliente");
            $respuesta->setStatusCode(404);
            return $respuesta;
        }
        //Si existe el cliente se devuelve esos datos
        $clienteDatos = $datosConsultaClienteObj->cliente['0'];
        $tamClienteDatos = strlen($clienteDatos->descripcion);
        if(is_null($clienteDatos->descripcion) or $tamClienteDatos<3){
            $respuesta=$this->responderConError("No existe el cliente");
            $respuesta->setStatusCode(404);
            return $respuesta;
        }
        $clienteRespuesta=new Cliente([
            "cli_documento"=>$documento,
            "cli_nombres"=>strtoupper($clienteDatos->descripcion),
            "cli_direccion"=>strtoupper($clienteDatos->direccionDomicilio),
            "cli_telefono"=>$clienteDatos->telefonoDomiclio,
            "cli_email"=>strtoupper($clienteDatos->correo),
            "IDTipoDocumento"=>$tipoDocumento
        ]);
        return $clienteRespuesta;
    }

    public function validarCliente(Request $request){
        $messages=[
            'IDTipoDocumento.required'=>'El campo es obligatorio (Tipo Documento).',
            'cli_nombres.required' => 'El campo es obligatorio (Nombre).',
            'cli_nombres.regex' => 'No se permiten números (Nombre).',
            'cli_nombres.max' => 'Se excedió la longitud máxima del campo (Nombre, :max caracteres).',
            'cli_nombres.min' => 'No se cumple la longitud minima del campo (Nombre, :min caracteres).',
            'max' => 'Se excedió la longitud máxima del campo (:attribute, :max caracteres).',
            'min' => 'No se cumple la longitud minima del campo (:attribute, :min caracteres).',
            'size' => 'Valor no válido (:attribute)',
            'cli_telefono.digits_between' =>'Se aceptan entre :min y :max dígitos (Teléfono)',
            'cli_telefono.required' => 'El campo es obligatorio (Teléfono).',
            'email' => 'No es un valor de email válido (Email)',
            'cli_documento.numeric' => 'El valor debe ser numérico (Documento)',
            'cli_documento.digits' => 'Se requieren exactamente :digits dígitos (Documento)',
            'cli_documento.regex' =>'No es un RUC válido (Documento)',
            'cli_direccion.required'=>'El valor es obligatorio (Dirección)',
            'cedula' => 'No es una cédula válida'

        ];

        $this->validate($request, [
            'IDTipoDocumento' => 'required|size:36',
        ],$messages);

        $objTipoDocumento=TipoDocumento::findOrFail($request->get('IDTipoDocumento'));

        $emailsinEspaciosLuegoDelPunto=preg_replace('^\. +^', '.', $request->cli_email);
        Input::merge(['cli_email'=>$emailsinEspaciosLuegoDelPunto]);

        if($objTipoDocumento->tpdoc_descripcion!=='CONSUMIDOR FINAL'){
            $this->validate($request, [
                'cli_nombres' => 'required | max:50 | min:5 | regex:/^[\pL\s\-]+/u',
                'cli_documento' => 'required',
                'cli_email' => 'nullable | email',
            ],$messages);
            if($objTipoDocumento->tpdoc_descripcion==='CEDULA'){
                $this->validate($request, [
                    'cli_documento' => 'numeric | digits:10 | cedula',
                    'cli_telefono' => 'required|digits_between:7,10',
                    'cli_direccion' => 'required|min:5',
                ],$messages);
            }
            if($objTipoDocumento->tpdoc_descripcion==='RUC'){
                $this->validate($request, [
                    'cli_documento' => 'numeric | digits:13 | regex:/^\d{10}001$/u',
                    'cli_telefono' => 'required|digits_between:7,10',
                    'cli_direccion' => 'required|min:5',
                ],$messages);
            }
        }
        $retorno=new \stdClass();
        $retorno->mensaje="OK";
        return Response()->api($retorno);
    }

    public function responderConError($mensaje){
        return  Response()->errorapi($mensaje);
    }
}
