<?php

namespace App\Http\Controllers\Api;

use App\Entities\maxpoint\MenuAgrupacion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Entities\Maxpoint\ProductosMenusKiosko;
use App\Entities\Maxpoint\Menu;
use Dingo\Api\Routing\Helpers;
use App\Entities\Maxpoint\Plus;
//use App\Transformers\PlusTransformer;
use App\Transformers\ProductosMenuKioskoTransformer;
use App\Transformers\MenuTransformer;
use League\Fractal;
use League\Fractal\Manager;

class MenuController extends Controller
{
    use Helpers;

    public function menus()
    {
        $fractal = new Manager();
        $fractal->parseExcludes('agrupaciones.plus.preguntas');
        $menuLocal=Menu::with(["menuAgrupaciones"=> function ($q) {
            $q->orderBy('mag_orden','asc');
        },"menuAgrupaciones.plus"])
        ->where("menu_Nombre","KIOSKO SALON")
        ->first();

        $data = new Fractal\Resource\Item($menuLocal, new MenuTransformer);

        $data->setMeta(["status"=>"success"]);
        $resource =$fractal->createData($data)->toArray();

        return  $resource;
    }

    public function preguntasSugeridas($pluid)
    {
        $fractal = new Manager();
        $plu=ProductosMenusKiosko::with(["preguntas"=> function ($q) {
            $q->orderBy('orden','asc');
        },"preguntas.respuestas"=> function ($q) {
            $q->orderBy('plu_id','asc');
        }])
            ->where("IDProducto",$pluid)
            ->firstorFail();
        $data = new Fractal\Resource\Item($plu, new ProductosMenuKioskoTransformer);
        $data->setMeta(["status"=>"success"]);
        $resource =$fractal->createData($data)->toArray();
        return  $resource;
    }
}
