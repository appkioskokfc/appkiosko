<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Entities\Maxpoint\FormaPago;
use App\Entities\Maxpoint\Menu;
use Dingo\Api\Routing\Helpers;

class FormasPagoController extends Controller
{
    use Helpers;
    public function formasPagoAppKiosko()
    {
        $formaPago=new FormaPago();
        $formasPagoAppKiosko=$formaPago->join(
            'Tipo_Forma_Pago',
            function ($join) {
                $join->on('FormaPago.IDTipoFormaPago', '=', 'Tipo_Forma_Pago.IDTipoFormaPago')
                    ->whereIn('Tipo_Forma_Pago.tfp_descripcion',['TARJETA DE CREDITO','TARJETA DE DEBITO']);
        })
            ->join('Status as e2', function ($join) {
                $join->on('Tipo_Forma_Pago.IDStatus', '=', 'e2.IDStatus')
                    ->whereIn('e2.mdl_id',[9])
                    ->where('e2.std_descripcion','=','Activo');
            })
            ->join('Status as e1', function ($join) {
            $join->on('FormaPago.IDStatus', '=', 'e1.IDStatus')
                ->whereIn('e1.mdl_id',[9])
                ->where('e1.std_descripcion','=','Activo');
        })->select(['FormaPago.fpf_codigo','FormaPago.fmp_descripcion'])
        ->get();
        return  Response()->api($formasPagoAppKiosko);
    }
}
