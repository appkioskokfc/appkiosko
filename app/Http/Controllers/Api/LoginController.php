<?php

namespace App\Http\Controllers\Api;

use App\Entities\Maxpoint\MenuAgrupacion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Entities\Maxpoint\ProductosMenusKiosko;
use App\Entities\Maxpoint\Menu;
//use Dingo\Api\Routing\Helpers;
use Response;
use App\Entities\Kiosko\KioskoEstaciones;
class LoginController extends Controller
{
    //use Helpers;
    protected function iniciarControlEstacion(Request $request)
    {
        $this->validate($request, [
            'clave' => 'required|min:5',
        ]);

        $ipPeticion=$request->ip();
        $objEstacion=KioskoEstaciones::where('ip', '=' ,$ipPeticion)->first();
        if(is_null($objEstacion)){
            $retorno=new \stdClass();
            $retorno->mensaje='No es una estación configurada';
            return Response::errorapi($retorno);
        }

        $ipEstacion=$objEstacion->ip;
        $claveUsuario = $objEstacion->clave_pos;
        $claveAdmin=$request->get('clave');

        $sqlIniciarVentas="EXECUTE dbo.kiosko_iniciarVentas '$ipEstacion', '$claveUsuario','$claveAdmin'";
        $resultado = DB::selectOne($sqlIniciarVentas);
        $retorno=new \stdClass();
        $retorno->mensaje=$resultado->mensaje;

        if($resultado->estado==0) return Response::api($retorno);
        return Response::errorapi($retorno);
    }

    protected function finalizarControlEstacion(Request $request)
    {
        $this->validate($request, [
            'clave' => 'required|min:5',
        ]);
        $ipPeticion=$request->ip();

        $objEstacion=KioskoEstaciones::where('ip', '=' ,$ipPeticion)->first();
        if(is_null($objEstacion)){
            $retorno=new \stdClass();
            $retorno->mensaje='No es una estación configurada';
            return Response::errorapi($retorno);
        }

        $ipEstacion=$objEstacion->ip;
        $usuarioPos=$objEstacion->usuario_pos;
        $claveAdmin = $request->get('clave');

        $sqlIniciarVentas="EXECUTE dbo.kiosko_FinalizarVentas '$ipEstacion', '$claveAdmin', '$usuarioPos'";

        $resultado = DB::selectOne($sqlIniciarVentas);
        $retorno=new \stdClass();
        $retorno->mensaje=$resultado->mensaje;

        if($resultado->estado==0) return Response::api($retorno);
        return Response::errorapi($retorno);
    }

    protected function validarCedulaAdministrador(Request $request,$cedula){
        $ipPeticion=$request->ip();

        $objEstacion=KioskoEstaciones::where('ip', '=' ,$ipPeticion)->first();
        if(is_null($objEstacion)){
            $retorno=new \stdClass();
            $retorno->mensaje='No es una estación configurada';
            return Response::errorapi($retorno);
        }

        $ipEstacion=$objEstacion->ip;
        $claveUsuario=$objEstacion->clave_pos;

        $sqlValidarCedulaAdmin="EXECUTE seguridad.USP_validaUsuarioAdmin 1,'$cedula', '$claveUsuario', '$ipEstacion','0'";
        $resultado = DB::selectOne($sqlValidarCedulaAdmin);

        $retorno=new \stdClass();
        $retorno->mensaje="OK";
        $retorno->esAdmin=(1==$resultado->admini)?true:false;

        return Response::api($retorno);
    }
	
	protected function estadoAsignacionKiosko(Request $request){
        $ipPeticion=$request->ip();
        $objEstacion=KioskoEstaciones::where('ip', '=' ,$ipPeticion)->first();
        if(is_null($objEstacion)){
            $retorno=new \stdClass();
            $retorno->mensaje='No es una estación configurada';
            return Response::errorapi($retorno);
        }

        $ipEstacion=$objEstacion->ip;
        $usuarioPos=$objEstacion->usuario_pos;
		 
		 $sqlValidarEstadoAsignacion="EXECUTE dbo.kiosko_VerificaControlEstacion '$ipEstacion', '$usuarioPos'";
		 $resultado = DB::selectOne($sqlValidarEstadoAsignacion);
		 if(!is_null($resultado)){
			 $estadoAsignacion = ($resultado->inicio == 1) ? true : false;
		 }else{
			 $estadoAsignacion = false;
		 }
		 $retorno=new \stdClass();
		 $retorno->mensaje="OK";
		 $retorno->isActive=$estadoAsignacion;
		 return Response::api($retorno);
	}
	
}
