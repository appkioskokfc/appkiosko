<?php

namespace App\HTTP\Controllers\Api;

use App\Entities\Kiosko\KioskoCabeceraPedido;
use App\Events\EventFs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Kiosko\KioskoDetallePedido;
use App\Entities\Maxpoint\TipoDocumento;
use App\Entities\Kiosko\KioskoEstaciones;
use App\Entities\Kiosko\KioskoLogPedidos;
use DB;
use Response;
use App\Utils\StringTools;
use Illuminate\Database\QueryException;
use Log;

class PedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return KioskoCabeceraPedido::all();
    }

    public function montosParciales(Request $request)
    {

        $this->validate($request, [
            'detalles' => 'required|array|min:1',
            'detalles.*.plu_id'=>'required|int',
            'detalles.*.dop_cantidad'=>'required|int',
        ],[
            'required' => 'El campo es obligatorio.',
            'integer' => 'El valor debe ser un número entero.',
        ]);
        $detalles=$request->get("detalles");
        $arrayStrDetalles=[];
        foreach($detalles as $det){
            $arrayStrDetalles[]=implode("*",$det);
        }
        $strListaProducto=implode("_",$arrayStrDetalles);
        $sqlTotalesParciales="[dbo].[kiosko_USP_TotalesParciales] :listaProductos";

        $resultado = \DB::selectOne(
            $sqlTotalesParciales,
            [
                'listaProductos' => $strListaProducto
            ]);
        $objrespuesta=new \stdClass();
        $objrespuesta->subtotal=round($resultado->subtotal,2);
        $objrespuesta->iva=round($resultado->iva,2);
        $objrespuesta->total=round($resultado->total,2);

        return  Response()->api($objrespuesta);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ipPeticion=$request->ip();

        $objEstacion=KioskoEstaciones::where('ip', '=' ,$ipPeticion)->first();
        if(is_null($objEstacion)){
            $retorno=new \stdClass();
            $retorno->mensaje='No es una estación configurada';
            return Response::errorapi($retorno);
        }

        $ipEstacion=$objEstacion->ip;
        $usuarioPos=$objEstacion->usuario_pos;

        $messages=[
            'required' => 'El campo [:attribute] es obligatorio.',
            'regex' => 'El campo [:attribute] no admite números.',
            'integer' => 'El campo [:attribute] debe ser un número entero.',
            'cli_nombres.max' => 'Se excedió la longitud máxima del campo [:attribute] (:max caracteres).',
            'cli_nombres.min' => 'No se cumple la longitud minima del campo [:attribute] (:min caracteres).',
            'size' => 'Valor no válido en [:attribute]',
            'detalles.array.min'=>'Se requiere al menos :min valor(es) en [:attribute]',
            'email' => 'El campo [:attribute] no es un valor de email válido',
            'cli_email.email' => 'No es un valor de email válido',
            'numeric' => 'El campo [:attribute] no es un valor numérico',
            'digits' => 'Se requieren exactamente :digits dígitos en el campo [:attribute]',
            'in' => 'Solo se aceptan los valores [:values] en el campo [:attribute]',
        ];

        $this->validate($request, [
            'IDTipoDocumento' => 'required|size:36',
            'cfac_subtotal' => 'required|numeric',
            'cfac_iva' => 'required|numeric',
            'cfac_total' => 'required|numeric',
            'tipo_servicio' => 'in:salon,llevar',
            'detalles' => 'required|array|min:1',
            'detalles.*.plu_id'=>'required|integer',
            'detalles.*.dop_cantidad'=>'required|integer',
            'detalles.*.modifica'=>'sometimes|integer|nullable',

            'formaspago' => 'required|array|min:1',
            'formaspago.*.bin'=>'required|integer|digits:6',

            'autorizaciones' => 'required|array|min:1',
            'autorizaciones.*.codigoComercio' => 'required',
            'autorizaciones.*.numeroTerminal' => 'required',
            'autorizaciones.*.lote' => 'required',
            'autorizaciones.*.referencia' => 'required',
            'autorizaciones.*.autorizacion' => 'required',
            'autorizaciones.*.tarjetaHabiente' => 'required',
            'autorizaciones.*.numeroTarjeta' => 'required',
            'autorizaciones.*.numAdquiriente' => 'required',
            'autorizaciones.*.codigoResultado' => 'required',
            'autorizaciones.*.mensajeResultado' => 'required',
        ],$messages);

        $tipoConsumidorFinal=TipoDocumento::where('tpdoc_descripcion', '=' ,'CONSUMIDOR FINAL')
            ->firstOrFail(['IDTipoDocumento'])->IDTipoDocumento;

        if($request->get('IDTipoDocumento')!==$tipoConsumidorFinal){
            $this->validate($request, [
                'cli_nombres' => 'required|max:50|min:5|regex:/^[\pL\s\-]+$/u',
                'cli_documento' => 'required',
            ],$messages);
            $datosInsercionFactura=$request->all();

            $emailsinEspaciosLuegoDelPunto=preg_replace('^\. +^', '.', $datosInsercionFactura['cli_email']);

            $datosInsercionFactura['cli_nombres']=strtoupper(StringTools::normalize($datosInsercionFactura['cli_nombres']));

            $direccionInput=isset($datosInsercionFactura['cli_direccion'])?strtoupper(StringTools::normalize($datosInsercionFactura['cli_direccion'])):null;
            $datosInsercionFactura['cli_direccion']=$direccionInput;
            $datosInsercionFactura['cli_email']=strtoupper($emailsinEspaciosLuegoDelPunto);

        }else{
            // Esto se debería hacer usando los Mutators de laravel, sencillamente no funcionó
            $datosInsercionFactura=$this->llenarConsumidorFinal($request->all());
        }

        $log=new KioskoLogPedidos();
        $log->ip=$ipPeticion;
        $textoPeticion=StringTools::minifyText($request->getContent());
        Log::info('Pedido Entrante: '.$textoPeticion);
        $log->json_texto=$textoPeticion;
        $log->save();

        $retorno=new \stdClass();

        try{
            $cabeceraCreada=KioskoCabeceraPedido::create($datosInsercionFactura);
            forEach($request->get("detalles") as $detalle ){
                $det=$cabeceraCreada->detalles()->create($detalle);
                if(isset($detalle["modificadores"])){
                    foreach($detalle["modificadores"] as $modificador){

                        $mod = $det->modificadores()->create($modificador);
                        $mod->id_orden=$cabeceraCreada->id;
                        $mod->save();
                    }
                }
            }
            $cabeceraCreada->formasPago()->createMany(
                $request->get("formaspago")
            );

            $cabeceraCreada->autorizacionesSwitch()->createMany(
                $request->get("autorizaciones")
            );

        }catch(QueryException $ex){
            $log->mensaje_error=$ex->getMessage();
            $log->estado="error";
            $log->save();
            $retorno->mensaje="No se procesó el pedido";
            return Response()->errorapi($retorno);
        }

        $sqlcrearPedidoMaxpoint="EXECUTE dbo.kiosko_USP_IntegracionInformacionPedido $cabeceraCreada->id, '$ipEstacion', '$usuarioPos'";
        try{
            $resultado = DB::selectOne($sqlcrearPedidoMaxpoint);
        }catch(QueryException $ex){
            $log->mensaje_error=$ex->getMessage();
            $log->estado="error";
            $log->save();
            $retorno->mensaje="No se procesó el pedido";
            return Response()->errorapi($retorno);
        }catch(\Exception $ex){
            $log->mensaje_error=$ex->getMessage();
            $log->estado="error";
            $log->save();
            $retorno->mensaje=$ex->getMessage();
            return Response()->errorapi($retorno);
        }
        if($resultado->codigo==1) {
            $log->mensaje_error=$resultado->mensaje;
            $log->estado="error";
            $log->save();
            $retorno->mensaje=$resultado->mensaje;
            return Response()->errorapi($retorno);
        }else{
            $log->delete();
        }
        $retorno->mensaje=$resultado->mensaje;
        $retorno->htmlFactura=preg_replace( "/\r|\n|\t/", "", $resultado->factura);
        $retorno->ordenPedido=preg_replace( "/\r|\n|\t/", "", $resultado->ordenPedido);
        $retorno->voucher=preg_replace( "/\r|\n|\t/", "", $resultado->voucher);
        return Response()->api($retorno);
    }

    /*
     * Setea los valor para el cliente consumidor Final //ACTUALMENTE EL CONSUMIDOR FINAL ESTÁ DESACTIVADO
     * ESTO QUEDA PARA SER USADO EN UN FUTURO
     */
    private function llenarConsumidorFinal($valores){
        // Esto se debería hacer usando los Mutators de laravel, sencillamente no funcionó
        $valores['cli_documento']='9999999999';
        $valores['cli_nombres']='CONSUMIDOR FINAL';
        $valores['cli_telefono']='2222222';
        $valores['cli_email']='consumidor.final@kfc.com.ec';
        $valores['cli_direccion']='Quito';
        return $valores;
    }
}