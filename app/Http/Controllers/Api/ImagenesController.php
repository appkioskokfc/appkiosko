<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\PromocionesTransformer;
use Illuminate\Http\Request;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Entities\Kiosko\KioskoPromociones;
use League\Fractal\Manager;
use Illuminate\Support\Facades\Storage;
use App\Serializers\ArraySerializer;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection as LaravelCollection;
use Response;

class ImagenesController extends Controller
{
    public function promociones(Request $request)
    {
        $fractal = new Manager();
        $promocionesActivas = KioskoPromociones::select()->where("estado", "=", "activo")
            ->with('plu')
            ->get();

        $data = new Collection($promocionesActivas, new PromocionesTransformer());

        $data->setMeta(["status" => "success"]);
        $resource = $fractal->parseExcludes("plu.preguntas")->createData($data)->toArray();
        return $resource;
    }


    public function historias(Request $request)
    {
        $files = Storage::disk('imagenes')->listContents("historias");

        $fractal = new Manager();
        $resource = new Collection($files, function (array $file) {
            return [
                'url' => Storage::disk('imagenes')->url($file["path"])
            ];
        });

        $resource->setMeta(["status" => "success"]);
        $respuesta = $fractal->createData($resource)->toJson();
        return $respuesta;
    }


    public function pantallaInicio(Request $request)
    {
        $files = Storage::disk('imagenes')->listContents("pantallainicio");

        $fractal = new Manager();
        $resource = new Collection($files, function (array $file) {
            return [
                'url' => Storage::disk('imagenes')->url($file["path"])
            ];
        });

        $resource->setMeta(["status" => "success"]);
        $respuesta = $fractal->createData($resource)->toJson();
        return $respuesta;
    }

    public function sincronizarAzureImagenesPantallaInicio(Request $request)
    {

        $directorio = public_path("images\pantallainicio");
        $fs = new Filesystem();
        if (!$fs->exists($directorio)) {
            $fs->makeDirectory($directorio);
        } else {
            $fs->cleanDirectory($directorio);
        }
        $files = Storage::disk('imagenes');
        $arrayImagenes = $request->get("imagenes");

        $correctos = new LaravelCollection;
        $errores = new LaravelCollection;
        foreach ($arrayImagenes as $imagen) {
            $contents = file_get_contents($imagen["url"]);
            try {
                $files->put("pantallainicio" . DIRECTORY_SEPARATOR . $imagen["nombre"], $contents);
                $correctos->push($imagen["nombre"]);
            } catch (\Exception $ex) {
                $errores->put([$imagen["nombre"] => $ex->getMessage()]);
            }
        }
        $estado = count($errores) > 0 ? "ERROR" : "OK";
        return [
            "estado" => $estado,
            "errores" => $errores,
            "correctos" => $correctos
        ];

    }
}
