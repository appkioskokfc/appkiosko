<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class ReportesController extends Controller {
    public function ventasSwitchKiosko(Request $request) {
        $fechaInicio=Carbon::createFromFormat("Ymd",$request->fechainicio);
        $fechaFin=Carbon::createFromFormat("Ymd",$request->fechafin);
        $sqlAutorizaciones="[dbo].[kiosko_reporteSwitch] :fechaInicio , :fechaFin";
        $autorizaciones =DB::select(
            $sqlAutorizaciones,
                [
                    'fechaInicio' => $fechaInicio->format('Y-m-d'),
                    'fechaFin' => $fechaFin->format('Y-m-d')
                ]
            );
        return $autorizaciones;
    }

}
