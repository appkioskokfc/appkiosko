<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use App\Utils\Validacion;
use App\Entities\Kiosko\KioskoDetallePedido;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('cedula',function ($attribute, $value, $parameters, $validator){
            $validador=new Validacion();
            return $validador->validarCedula($value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
