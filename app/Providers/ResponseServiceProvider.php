<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;

class ResponseServiceProvider extends ServiceProvider
{
    public function boot(ResponseFactory $factory)
    {
        $factory->macro('api', function ($data) use ($factory) {
            $customFormat = [
                'data' => $data,
                'meta'=>[
                    'status' => 'success'
                ]
            ];
            return $factory->make($customFormat);
        });

        $factory->macro('errorapi', function ($data) use ($factory) {
            $customFormat = [
                'data' => $data,
                'meta'=>[
                    'status' => 'error'
                ]
            ];
            return $factory->make($customFormat)->setStatusCode(400);
        });
    }

    public function register(){}
}