<?php
/**
 * Created by PhpStorm.
 * User: fabricio.sierra
 * Date: 1/22/2018
 * Time: 11:52 AM
 */

namespace App\Serializers;

class SelectsSerializer extends \League\Fractal\Serializer\ArraySerializer
{
    /**
     * Serialize a collection.
     *
     * @param string $resourceKey
     * @param array  $data
     *
     * @return array
     */
    public function collection($resourceKey, array $data)
    {
        return ["Result"=>"OK","Options"=>$data];
    }

    public function item($resourceKey, array $data)
    {
        return ["Result"=>"OK","Options"=>$data];
    }
}