<?php
namespace App\Transformers\Admin;

use App\Entities\Maxpoint\UsersPos;
use League\Fractal;

class UsersPosSelectTransformer extends Fractal\TransformerAbstract
{
    public function transform(UsersPos $user)
    {
        return [
            "DisplayText" => $user->usr_descripcion,
            "Value" => $user->usr_nombre_en_pos
        ];
    }
}