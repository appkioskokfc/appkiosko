<?php
namespace App\Transformers\Admin;
use App\Entities\Maxpoint\ProductosMenusKiosko;
use App\Transformers\PreguntaSugeridaTransformer;
use Carbon\Carbon;
use League\Fractal;

class PlusKioskoTransformer extends Fractal\TransformerAbstract
{
    public function transform(ProductosMenusKiosko $plus)
    {
        $timestamp=Carbon::now()->timestamp;
        return [
            "identificador"=>$plus->IDProducto,
            "nombre"=>$plus->Nombre,
            "foto"=>$plus->urlFoto.'?v='.$timestamp
        ];
    }

    public function includePreguntas(ProductosMenusKiosko $plus)
    {
        $preguntas = $plus->preguntas;
        return $this->collection($preguntas,new PreguntaSugeridaTransformer);
    }
}