<?php
namespace App\Transformers\Admin;
use App\Entities\Kiosko\KioskoEstaciones;
use Carbon\Carbon;
use League\Fractal;

class EstacionKioskoTransformer extends Fractal\TransformerAbstract
{
    public function transform(KioskoEstaciones $estacion)
    {
        return [
            "usuario_pos"=>$estacion->user->IdUserspos,
            "ip"=>$estacion->ip
        ];
    }
}