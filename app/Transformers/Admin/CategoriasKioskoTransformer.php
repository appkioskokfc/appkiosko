<?php
namespace App\Transformers\Admin;
use App\Entities\Maxpoint\MenuAgrupacion;
use App\Transformers\PreguntaSugeridaTransformer;
use Carbon\Carbon;
use League\Fractal;

class CategoriasKioskoTransformer extends Fractal\TransformerAbstract
{
    public function transform(MenuAgrupacion $categoria)
    {
        $timestamp=Carbon::now()->timestamp;
        return [
            "identificador"=>$categoria->IDMenuAgrupacion,
            "nombre"=>$categoria->mag_descripcion,
            "foto"=>$categoria->urlFoto.'?v='.$timestamp
        ];
    }
}