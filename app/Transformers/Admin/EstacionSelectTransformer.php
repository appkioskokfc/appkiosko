<?php
namespace App\Transformers\Admin;

use App\Entities\Maxpoint\Estacion;
use League\Fractal;

class EstacionSelectTransformer extends Fractal\TransformerAbstract
{
    public function transform(Estacion $estacion)
    {
       return [
            "DisplayText" => $estacion->est_ip,
           "Value" => $estacion->est_ip
        ];
    }
}