<?php
namespace App\Transformers;
use App\Entities\Maxpoint\Plus;
use League\Fractal;

class PlusTransformer extends Fractal\TransformerAbstract
{
//    protected $defaultIncludes = [
//        'author'
//    ];

    public function transform(Plus $plus)
    {
        //dd($plus->preguntas());
        return [
            "IDProducto"=>$plus->IDProducto,
            "TipoProducto"=>$plus->TipoProducto,
            "Impuesto"=>$plus->Impuesto,
            "Nombre"=>$plus->Nombre,
            "NombreImpresion"=>$plus->NombreImpresion,
            "HorarioRestaurante"=>$plus->HorarioRestaurante,
            "DescripcionProducto"=>$plus->DescripcionProducto,
            "pr_valor_neto"=>round($plus->pr_valor_neto,2),
            "pr_valor_iva"=>round($plus->pr_valor_iva,2),
            "pr_pvp"=>round($plus->pr_pvp,2),
        ];
    }
}