<?php
namespace App\Transformers;

use App\Entities\Kiosko\KioskoPromociones;
use League\Fractal\TransformerAbstract;
use App\Transformers\ProductosMenuKioskoTransformer;
use App\Entities\Maxpoint\ProductosMenusKiosko;
class PromocionesTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'plu'
    ];

    public function transform(KioskoPromociones $promocion)
    {
       return [
            "url_foto" => $promocion->urlFoto,
        ];
    }
    public function includePlu(KioskoPromociones $promocion)
    {
        return $this->item($promocion->plu, new ProductosMenuKioskoTransformer());
    }
}