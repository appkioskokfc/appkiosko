<?php

namespace App\Transformers;

use App\Entities\Maxpoint\TipoDocumento;
use League\Fractal;

class TipoDocumentoTransformer extends Fractal\TransformerAbstract {

    public function transform(TipoDocumento $documento) {
        return [
            "IDTipoDocumento" => $documento->IDTipoDocumento,
            "tpdoc_descripcion" => $documento->tpdoc_descripcion,
        ];
    }

}
