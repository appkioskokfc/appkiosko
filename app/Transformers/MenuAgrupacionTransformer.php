<?php
namespace App\Transformers;
use App\Entities\Maxpoint\MenuAgrupacion;
use App\Transformers\ProductosMenuKioskoTransformer;
use League\Fractal;
use Carbon\Carbon;

class MenuAgrupacionTransformer extends Fractal\TransformerAbstract
{
    protected $defaultIncludes = [
        'plus'
    ];

    public function transform(MenuAgrupacion $menuAgrupacion)
    {
       // dd($menuAgrupacion);
        $timestamp=Carbon::now()->timestamp;
        return [
            "IDMenuAgrupacion" => $menuAgrupacion->IDMenuAgrupacion,
            "cdn_id" => $menuAgrupacion->cdn_id,
            "mag_descripcion" => $menuAgrupacion->mag_descripcion,
            "mag_num_agr_producto" => $menuAgrupacion->mag_num_agr_producto,
            "mag_orden" => (int)$menuAgrupacion->mag_orden,
            "foto" => $menuAgrupacion->urlFoto.'?v='.$timestamp
       ];
    }
    public function includePlus(MenuAgrupacion $menuAgrupacion)
    {
        $plus = $menuAgrupacion->plus;
        return $this->collection($plus, new ProductosMenuKioskoTransformer);
    }
}