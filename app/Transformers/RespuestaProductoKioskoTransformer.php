<?php
namespace App\Transformers;
use App\Entities\Maxpoint\RespuestaProductoKiosko;
use App\Transformers\PreguntaSugeridaTransformer;
use League\Fractal;

class RespuestaProductoKioskoTransformer extends Fractal\TransformerAbstract
{

    public function transform(RespuestaProductoKiosko $plus)
    {
       // dd($plus);
        return [
            "IDProducto"=>$plus->IDProducto,
            "TipoProducto"=>$plus->TipoProducto,
            "Impuesto"=>$plus->Impuesto,
            "Nombre"=>$plus->Nombre,
            "NombreImpresion"=>$plus->NombreImpresion,
            "HorarioRestaurante"=>$plus->HorarioRestaurante,
            "DescripcionProducto"=>$plus->DescripcionProducto,
            "pr_valor_neto"=>round($plus->PVP,2),
            "pr_valor_iva"=>round($plus->Iva,2),
            "pr_pvp"=>round($plus->Neto,2),
        ];
    }
}