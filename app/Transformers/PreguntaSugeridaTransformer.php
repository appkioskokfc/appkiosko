<?php
namespace App\Transformers;
use App\Entities\Maxpoint\Pregunta_Sugerida;
use League\Fractal;
use App\Transformers\RespuestaProductoKioskoTransformer;
class PreguntaSugeridaTransformer extends Fractal\TransformerAbstract
{
    protected $defaultIncludes = [
        'respuestas'
    ];

    public function transform(Pregunta_Sugerida $pregunta)
    {
        return [
            'id'      => $pregunta->IDPreguntaSugerida,
            'descripcion' => trim($pregunta->psug_descripcion_pos),
            'resp_minima' =>(int)$pregunta->psug_resp_minima,
            'psug_resp_maxima'=>(int)$pregunta->psug_resp_maxima,
            'descripcion_pos'=>trim($pregunta->psug_descripcion_pos),
            'orden'=>(int)$pregunta->pivot->orden,
        ];
    }
    public function includeRespuestas(Pregunta_Sugerida $pregunta)
    {
        $respuestas = $pregunta->respuestas;
        return $this->collection($respuestas, new RespuestaProductoKioskoTransformer);
    }
}