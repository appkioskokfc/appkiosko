<?php
namespace App\Transformers;
use App\Entities\Maxpoint\ProductosMenusKiosko;
use App\Entities\Maxpoint\Pregunta_Sugerida;
use App\Transformers\PreguntaSugeridaTransformer;
use Carbon\Carbon;
use League\Fractal;

class ProductosMenuKioskoTransformer extends Fractal\TransformerAbstract
{
    protected $defaultIncludes = [
        'preguntas'
    ];

    public function transform(ProductosMenusKiosko $plus)
    {
        $timestamp=Carbon::now()->timestamp;
        return [
            "IDProducto"=>$plus->IDProducto,
            "TipoProducto"=>$plus->TipoProducto,
            "Impuesto"=>$plus->Impuesto,
            "Nombre"=>$plus->Nombre,
            "NombreImpresion"=>$plus->NombreImpresion,
            "HorarioRestaurante"=>$plus->HorarioRestaurante,
            "DescripcionProducto"=>$plus->DescripcionProducto,
            "pr_valor_neto"=>$plus->pr_valor_neto,
            "pr_valor_iva"=>$plus->pr_valor_iva,
            "pr_pvp"=>$plus->pr_pvp,
            "foto"=> $plus->urlFoto.'?v='.$timestamp
        ];
    }

    public function includePreguntas(ProductosMenusKiosko $plus)
    {
        $preguntas = $plus->preguntas;
        return $this->collection($preguntas,new PreguntaSugeridaTransformer);
    }
}