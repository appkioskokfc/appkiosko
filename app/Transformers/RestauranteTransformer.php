<?php
namespace App\Transformers;

use App\Entities\Maxpoint\Restaurante;
use League\Fractal;

class RestauranteTransformer extends Fractal\TransformerAbstract
{
    public function transform(Restaurante $restaurante)
    {
       return [
        "Merchantid" => $restaurante->rst_id
        ];
    }
}