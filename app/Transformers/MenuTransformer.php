<?php
namespace App\Transformers;
use App\Entities\Maxpoint\Menu;
use League\Fractal;

class MenuTransformer extends Fractal\TransformerAbstract
{
    protected $defaultIncludes = [
        'agrupaciones'
    ];

    public function transform(Menu $menu)
    {
       return [
        "IDMenu" => $menu->IDMenu,
        "cdn_id" => $menu->cdn_id,
        "menu_Nombre" =>$menu->menu_Nombre,
        ];
    }

    public function includeAgrupaciones(Menu $menu)
    {

        $agrupaciones = $menu->menuAgrupaciones->sortBy('mag_orden');
        return $this->collection($agrupaciones, new MenuAgrupacionTransformer);
    }
}