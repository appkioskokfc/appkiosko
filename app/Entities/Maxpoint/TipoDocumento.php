<?php

namespace App\Entities\Maxpoint;

use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model {

    protected $table = 'Tipo_Documento';
    protected $primaryKey = 'IDTipoDocumento'; // or null
    public $incrementing = false;
    
     
}
