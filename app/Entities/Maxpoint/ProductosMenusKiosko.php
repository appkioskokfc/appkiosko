<?php

namespace App\Entities\Maxpoint;

use Illuminate\Database\Eloquent\Model;

class ProductosMenusKiosko extends Model
{
    protected $table="webservices.ProductosMenusKiosko";
    protected $primaryKey = 'IDProducto'; // or null
    protected $fillable=[
        "IDProducto",
        "TipoProducto",
        "Impuesto",
        "Nombre",
        "NombreImpresion",
        "HorarioRestaurante",
        "DescripcionProducto",
        "pr_valor_neto",
        "pr_valor_iva",
        "pr_pvp",
    ];
    protected $guarded=[
        "IDMenuAgrupacion",
        "IDCadena",
        "Estado",
        ];

    public function preguntas()
    {
        return $this->belongsToMany(
            'App\Entities\Maxpoint\Pregunta_Sugerida',
            'Plu_Pregunta',
            'plu_id',
            'IDPreguntaSugerida')
            ->withPivot('orden');
    }

    public function getFotoAttribute(){
        return str_slug(($this->Nombre.'_'.$this->IDProducto)).'.png';
    }

    public function getUrlFotoAttribute(){
        return asset('images/menu/products')."/".$this->foto;
    }

    public function getUbicacionFotoAttribute(){
        /**
         * Crea un String con la ubicacion física del archivo, por ejemplo:
         * C:\laragon\www\appkiosko\public\images\menu\products\combo-completo-26910.png
         * Se utiliza la constante DIRECTORY_SEPARATOR para generar un string independiente de la plataforma
         * donde se instale la aplicacion
         */
        return join(DIRECTORY_SEPARATOR,[$this->directorioAlmacenamiento,$this->foto]);
    }

    public function getDirectorioAlmacenamientoAttribute(){
        /**
         * Crea un String con la ubicacion donde se guardarán las imagenes
         * C:\laragon\www\appkiosko\public\images\menu\products
         * Se utiliza la constante DIRECTORY_SEPARATOR para generar un string independiente de la plataforma
         * donde se instale la aplicacion
         */
        return join(DIRECTORY_SEPARATOR,[public_path(), "images","menu","products"]);
    }
}
