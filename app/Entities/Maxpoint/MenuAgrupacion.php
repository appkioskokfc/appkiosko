<?php

namespace App\Entities\maxpoint;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Maxpoint\Menu;

/**
 * App\Entities\maxpoint\MenuAgrupacion
 *
 * @property-read \App\Entities\Maxpoint\Menu $menu
 * @mixin \Eloquent
 */
class MenuAgrupacion extends Model
{
    protected $table="Menu_Agrupacion";
    protected $primaryKey = 'IDMenuAgrupacion'; // or null

    public $incrementing = false;

    protected $visible=[
        "IDMenuAgrupacion",
        "mag_descripcion",
        "mag_num_agr_producto",
        "mag_orden",
        "mag_nivel",
        "mag_padre",
        "plus"
    ];

   // protected $plus;

    public function plus(){
        $todoslosPlus = $this->hasMany('App\Entities\Maxpoint\ProductosMenusKiosko','IDMenuAgrupacion','IDMenuAgrupacion');
        if(Carbon::TUESDAY !== Carbon::now()->dayOfWeek) $todoslosPlus->where("Nombre","<>","MARTES LOCO");
        return $todoslosPlus;
    }

    public function getFotoAttribute(){
        return str_slug($this->mag_descripcion).'.png';
    }

    public function getUrlFotoAttribute(){
        return asset('images/menu/categories')."/".$this->foto;
    }

    public function getUbicacionFotoAttribute(){
        /**
         * Crea un String con la ubicacion física del archivo, por ejemplo:
         * C:\laragon\www\appkiosko\public\images\menu\products\combo-completo-26910.png
         * Se utiliza la constante DIRECTORY_SEPARATOR para generar un string independiente de la plataforma
         * donde se instale la aplicacion
         */
        return join(DIRECTORY_SEPARATOR,[$this->directorioAlmacenamiento,$this->foto]);
    }

    public function getDirectorioAlmacenamientoAttribute(){
        /**
         * Crea un String con la ubicacion donde se guardarán las imagenes
         * C:\laragon\www\appkiosko\public\images\menu\products
         * Se utiliza la constante DIRECTORY_SEPARATOR para generar un string independiente de la plataforma
         * donde se instale la aplicacion
         */
        return join(DIRECTORY_SEPARATOR,[public_path(), "images","menu","categories"]);
    }
}
