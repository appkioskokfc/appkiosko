<?php
/**
 * Created by PhpStorm.
 * User: fabricio.sierra
 * Date: 10/10/2017
 * Time: 10:42
 */

namespace App\Entities\Maxpoint;

use Illuminate\Database\Eloquent\Model;
class FormaPago extends Model
{
    protected $table="Formapago";
    protected $primaryKey = 'IDFormapago'; // or null

    public $incrementing = false;

    /**
     * Tipo de forma de pago.
     */
    public function tipoFormaPago()
    {
        return $this->belongsTo('App\Entities\Maxpoint\TipoFormaPago');
    }
}