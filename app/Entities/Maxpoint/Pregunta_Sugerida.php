<?php
/**
 * Created by PhpStorm.
 * User: fabricio.sierra
 * Date: 6/10/2017
 * Time: 16:57
 */

namespace App\Entities\Maxpoint;


use Illuminate\Database\Eloquent\Model;

class Pregunta_Sugerida extends Model
{
    protected $table="Pregunta_Sugerida";
    protected $primaryKey="IDPreguntaSugerida";
    public $incrementing = false;

//    protected $fillable=[
//        "pre_sug_descripcion",
//        "psug_resp_minima",
//        "psug_resp_maxima",
//        "psug_descripcion_pos"
//    ];
//    protected $visible=[
//        "pre_sug_descripcion"=>"descripcion",
//        "psug_resp_minima",
//        "psug_resp_maxima",
//        "psug_descripcion_pos"
//    ];

    public function respuestas(){
        return $this->hasManyThrough('App\Entities\Maxpoint\RespuestaProductoKiosko','App\Entities\Maxpoint\Respuestas','IDPreguntaSugerida','IDProducto','IDPreguntaSugerida');
    }
}