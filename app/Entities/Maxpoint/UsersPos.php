<?php

namespace App\Entities\maxpoint;

use Illuminate\Database\Eloquent\Model;

class UsersPos extends Model
{
    protected $table="users_pos";
    protected $primaryKey = 'IDUsersPos'; // or null

    public $incrementing = false;

}
