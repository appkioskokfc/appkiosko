<?php
/**
 * Created by PhpStorm.
 * User: fabricio.sierra
 * Date: 10/10/2017
 * Time: 10:27
 */

namespace App\Entities\Maxpoint;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table="Status";
    protected $primaryKey = 'IDStatus'; // or null
}