<?php

namespace App\Entities\maxpoint;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Maxpoint\Menu;

/**
 * App\Entities\maxpoint\Clasificacion
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Maxpoint\Menu[] $menus
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Maxpoint\Plus[] $plus
 * @mixin \Eloquent
 */
class Clasificacion extends Model
{
    protected $table="Clasificacion";
    protected $primaryKey = 'IDClasificacion'; // or null

    public $incrementing = false;

    public function menus()
    {
        return $this->hasMany('App\Entities\Maxpoint\Menu','IDClasificacion','IDClasificacion');
    }

    public function plus()
    {
        return $this->hasMany('App\Entities\Maxpoint\Plus','IDClasificacion','IDClasificacion');
    }
}
