<?php

namespace App\Entities\Maxpoint;

use Illuminate\Database\Eloquent\Model;

class Plus extends Model
{
    protected $table="Plus";
    protected $primaryKey = 'plu_id'; // or null

    public $incrementing = true;
    protected $hidden=["IDMenuAgrupacion"];

    protected $fillable=[
        "IDProducto",
        "TipoProducto",
        "Impuesto",
        "Nombre",
        "NombreImpresion",
        "HorarioRestaurante",
        "DescripcionProducto",
        "pr_valor_neto",
        "pr_valor_iva",
        "pr_pvp",
    ];

    public function clasificacion()
    {
        return $this->belongsTo('App\Entities\Maxpoint\Clasificacion', 'IDClasificacion');
    }

    public function preguntas()
    {
        return $this->belongsToMany(
            'App\Entities\Maxpoint\Pregunta_Sugerida',
            'Plu_Pregunta',
            'plu_id',
            'IDPreguntaSugerida')
            ->withPivot('orden');
    }
}
