<?php
/**
 * Created by PhpStorm.
 * User: fabricio.sierra
 * Date: 10/10/2017
 * Time: 10:22
 */

namespace App\Entities\Maxpoint;
use Illuminate\Database\Eloquent\Model;

class Restaurante extends Model
{
    protected $table="Restaurante";
    protected $primaryKey = 'rst_id'; // or null
}