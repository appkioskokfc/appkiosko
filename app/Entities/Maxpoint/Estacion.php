<?php

namespace App\Entities\maxpoint;

use Illuminate\Database\Eloquent\Model;

class Estacion extends Model
{
    protected $table="Estacion";
    protected $primaryKey = 'IDEstacion'; // or null

    public $incrementing = false;

}
