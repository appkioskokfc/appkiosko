<?php
/**
 * Created by PhpStorm.
 * User: fabricio.sierra
 * Date: 10/10/2017
 * Time: 10:42
 */

namespace App\Entities\Maxpoint;

use Illuminate\Database\Eloquent\Model;
class Cliente extends Model
{
    protected $table="Cliente";
    protected $primaryKey = 'IDCliente'; // or null

    public $incrementing = false;

    protected $fillable=[
        "cli_documento",
        "cli_nombres",
        "cli_direccion",
        "cli_telefono",
        "cli_email",
        "IDTipoDocumento"
    ];

    protected $visible=[
        "cli_documento",
        "cli_nombres",
        "cli_direccion",
        "cli_telefono",
        "cli_email",
        "IDTipoDocumento"
    ];

    /**
     * Tipo de forma de pago.
     */
    public function tipoDocumento()
    {
        return $this->belongsTo('App\Entities\Maxpoint\TipoDocumento','Id');
    }
}