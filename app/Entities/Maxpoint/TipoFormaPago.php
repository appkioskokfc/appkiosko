<?php
class TipoFormaPago extends Model
{
    protected $table='Tipo_Forma_Pago';
    protected $primaryKey = 'IDTipoFormaPago'; // or null

    public $incrementing = false;

    protected $hidden=[
        "IDStatus",
    ];

    /**
     * Formas de pago de este tipo.
     */
    public function formasPago()
    {
        return $this->hasMany('App\Entities\Maxpoint\FormaPago');
    }
}