<?php
/**
 * Created by PhpStorm.
 * User: fabricio.sierra
 * Date: 10/10/2017
 * Time: 10:23
 */

namespace App\Entities\Maxpoint;
use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    protected $table="Modulos";
    protected $primaryKey = 'mdl_id'; // or null
}