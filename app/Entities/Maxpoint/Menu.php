<?php

namespace App\Entities\Maxpoint;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Maxpoint\Menu
 *
 * @property-read \App\Entities\maxpoint\Clasificacion $clasificacion
 * @mixin \Eloquent
 */
class Menu extends Model
{
    protected $table='Menu';
    protected $primaryKey = 'IDMenu'; // or null

    public $incrementing = false;

    protected $hidden=[
        "IDStatus",
    ];

    public function menuAgrupaciones()
    {

        return $this->belongsToMany('App\Entities\Maxpoint\MenuAgrupacion' ,'MenuCategorias', 'IDMenu', 'IDMenuAgrupacion')
            ->withPivot('mag_orden');

    }
}
