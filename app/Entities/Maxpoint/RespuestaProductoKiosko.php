<?php

namespace App\Entities\Maxpoint;

use Illuminate\Database\Eloquent\Model;

class RespuestaProductoKiosko extends Model
{
    protected $table="webservices.RespuestaProductoKiosko";
    protected $primaryKey = 'IDProducto'; // or null
    protected $fillable=[
        "IDProducto",
        "TipoProducto",
        "Impuesto",
        "Nombre",
        "NombreImpresion",
        "HorarioRestaurante",
        "DescripcionProducto",
        "PVP",
        "Iva",
        "Neto",
    ];

    public function preguntas()
    {
        return $this->belongsToMany(
            'App\Entities\Maxpoint\Pregunta_Sugerida',
            'Plu_Pregunta',
            'plu_id',
            'IDPreguntaSugerida')
            ->withPivot('orden');
    }
}
