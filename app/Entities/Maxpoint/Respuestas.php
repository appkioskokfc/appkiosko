<?php
/**
 * Created by PhpStorm.
 * User: fabricio.sierra
 * Date: 6/10/2017
 * Time: 16:57
 */

namespace App\Entities\Maxpoint;
use Illuminate\Database\Eloquent\Model;

class Respuestas extends Model
{
    protected $table="Respuestas";
    protected $primaryKey = 'plu_id'; // or null
    public $incrementing = false;
}