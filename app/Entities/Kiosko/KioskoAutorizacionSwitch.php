<?php

namespace App\Entities\Kiosko;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Kiosko\KioskoCabeceraPedido;

/**
 * App\Entities\Kiosko\KioskoAutorizacionSwitch
 *
 * @property-read \App\Entities\Kiosko\KioskoCabeceraPedido $cabecera_pedido
 * @mixin \Eloquent
 */
class KioskoAutorizacionSwitch extends KioskoModel
{
    protected $table="kiosko_autorizaciones_switch";

    protected $fillable=[
        'codigoComercio',
        'numeroTerminal',
        'lote',
        'referencia',
        'autorizacion',
        'tarjetaHabiente',
        'numeroTarjeta',
        'numAdquiriente',
        'codigoResultado',
        'mensajeResultado'
    ];

    public function cabecera_pedido(){
        return $this->belongsTo("App\Entities\Kiosko\KioskoCabeceraPedido","idOrden");
    }
}
