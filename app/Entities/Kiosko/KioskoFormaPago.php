<?php

namespace App\Entities\Kiosko;

//use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Kiosko\KioskoFormaPago
 *
 * @mixin \Eloquent
 */
class KioskoFormaPago extends KioskoModel
{
    protected $fillable=[
        'bin',
        'fpf_total_pagar',
    ];
    //public function cabecera_pedido(){
    //    return $this->belongsTo("Kiosko\KioskoCabeceraPedido");
    //}
}
