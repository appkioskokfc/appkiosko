<?php

namespace App\Entities\Kiosko;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Kiosko\KioskoModel
 *
 * @mixin \Eloquent
 */
class KioskoModel extends Model
{
    public function getDateFormat()
    {
        return env("FORMATO_FECHAS","Y-m-d H:i:s.u");
    }

    public function fromDateTime($value)
    {
        return substr(parent::fromDateTime($value), 0, -3);
    }
}
