<?php

namespace App\Entities\Kiosko;

//use Illuminate\Database\Eloquent\Model;
/**
 * App\Entities\Kiosko\KioskoCabeceraPedido
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Kiosko\KioskoAutorizacionSwitch[] $autorizacionesSwitch
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Kiosko\KioskoDetallePedido[] $detalles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Kiosko\KioskoFormaPago[] $formasPago
 * @mixin \Eloquent
 */
class KioskoCabeceraPedido extends KioskoModel
{
    protected $fillable=[
        'cli_nombres',
        'IDTipoDocumento',
        'cli_documento',
        'cli_telefono',
        'cli_direccion',
        'cli_email',
        'cfac_subtotal',
        'cfac_iva',
        'cfac_total',
        'tipo_servicio',
    ];

    public function detalles(){
        return $this->hasMany("App\Entities\Kiosko\KioskoDetallePedido","id_orden");
    }

    public function formasPago(){
        return $this->hasMany("App\Entities\Kiosko\KioskoFormaPago","idOrden");
    }

    public function autorizacionesSwitch(){
        return $this->hasMany("App\Entities\Kiosko\KioskoAutorizacionSwitch","idOrden");
    }

    public function setCliNombres($value)
    {
        $this->attributes['cli_nombres'] = $value ?: "CONSUMIDOR FINAL";
    }
}
