<?php

namespace App\Entities\Kiosko;

//use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Kiosko\KioskoDetallePedido
 *
 * @property-read \App\Entities\Kiosko\KioskoCabeceraPedido $cabecera_pedido
 * @mixin \Eloquent
 */
class KioskoDetallePedido extends KioskoModel
{
    protected $fillable=[
        'plu_id',
        'dop_cantidad',
        'modifica',
    ];
    public function cabecera_pedido(){
        return $this->belongsTo("App\Entities\Kiosko\KioskoCabeceraPedido");
    }

    public function modifica()
    {
        return $this->belongsTo('App\Entities\Kiosko\KioskoDetallePedido', 'id');
    }

    public function modificadores(){
        return $this->hasMany("App\Entities\Kiosko\KioskoDetallePedido","modifica");
    }
}
