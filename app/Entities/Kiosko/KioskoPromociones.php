<?php

namespace App\Entities\Kiosko;


/**
 * App\Entities\Kiosko\KioskoLogPedidos
 *
 * @mixin \Eloquent
 */
class KioskoPromociones extends KioskoModel
{

    public function plu()
    {
        return $this->belongsTo('App\Entities\Maxpoint\ProductosMenusKiosko','plu_id');
    }

    public function getUrlFotoAttribute(){
        return asset('images/promociones')."/".$this->nombre_imagen;
    }

    public function getUbicacionFotoAttribute(){
        /**
         * Crea un String con la ubicacion física del archivo, por ejemplo:
         * C:\laragon\www\appkiosko\public\images\menu\products\combo-completo-26910.png
         * Se utiliza la constante DIRECTORY_SEPARATOR para generar un string independiente de la plataforma
         * donde se instale la aplicacion
         */
        return join(DIRECTORY_SEPARATOR,[$this->directorioAlmacenamiento,$this->nombreImagen]);
    }

    public function getDirectorioAlmacenamientoAttribute(){
        /**
         * Crea un String con la ubicacion donde se guardarán las imagenes
         * C:\laragon\www\appkiosko\public\images\menu\products
         * Se utiliza la constante DIRECTORY_SEPARATOR para generar un string independiente de la plataforma
         * donde se instale la aplicacion
         */
        return join(DIRECTORY_SEPARATOR,[public_path(), "images","promociones"]);
    }

}
