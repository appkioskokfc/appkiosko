<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiFormasPagoTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTiposDocumento()
    {
        $response = $this->json('GET', '/api/tiposdocumento',[],[
            "Authorization"=>"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMsImlzcyI6Imh0dHA6Ly8xNzIuMTcuMC40OjgwOS9hcGkvbG9naW4iLCJpYXQiOjE1MTY4OTMwNTQsImV4cCI6MTUxNjk2NTA1NCwibmJmIjoxNTE2ODkzMDU0LCJqdGkiOiJHTXNxWUY0andwN0diTDFyIn0.owZlFJTqSRUBuSikI8CiAwZITUy3XuKeAx-QinSGiDE",
            "Accept"=>"application/javascript"
        ]);
        $response
           ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' =>  ["IDTipoDocumento","tpdoc_descripcion"]
                ]
            ]);
    }
}
