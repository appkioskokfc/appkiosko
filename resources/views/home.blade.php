@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="row">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="col-sm-3 col-md-2">
                        <div class="thumbnail" style="text-align: center">
                            <div class="glyphicon glyphicon-phone" style="font-size: 5em"></div>
                            <div><a href="{{route("web_estaciones_listar")}}" class="btn btn-success">Estaciones</a></div>
                        </div>
                    </div>
                        <div class="col-sm-3 col-md-2">
                            <div class="thumbnail" style="text-align: center">
                                <div class="glyphicon glyphicon-camera" style="font-size: 5em"></div>
                                <div><a href="{{route("web_imagenes_listar")}}" class="btn btn-success">Imágenes</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
