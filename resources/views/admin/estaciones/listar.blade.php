@extends('layouts.app')
@section('styles')
    <link href="{{ asset('js/libs/jquery-ui/jquery-ui.css') }}" rel="stylesheet">
    <link href="{{ asset('js/libs/jtable/themes/metro/lightgray/jtable.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Estaciones</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12" id="contenedor-jtable"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascripts')
    <script src="{{ asset('js/libs/jquery-ui/jquery-ui.js') }}"></script>
    <script src="{{ asset('js/libs/jtable/jquery.jtable.js') }}"></script>
    <script src="{{ asset('js/libs/jtable/localization/jquery.jtable.es.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var $jtable=$('#contenedor-jtable');
            $jtable.jtable({
                title: 'Estaciones Kiosko',
                actions: {
                    listAction: '{{ route('web_estaciones_listar_ajax',[],false) }}',
                    createAction: '{{ route('web_estaciones_crear',[],false) }}',
                    updateAction: '{{ route('web_estaciones_editar',[],false) }}',
                    deleteAction: '{{ route('web_estaciones_eliminar',[],false) }}'
                },
                fields: {
                    id: {
                        key: true,
                        list: false
                    },
                    clave_pos: {
                        title: 'Clave POS',
                        width: '33%',
                        edit: false,
                        create: false,
                        list: false
                    },
                    ip: {
                        title: 'IP Estación',
                        options: '{{ route('web_estaciones_maxpoint',[],false) }}',
                        width: '33%'
                    },
                    usuario_pos: {
                        title: 'Usuario POS',
                        options: '{{ route('web_usuarios_maxpoint',[],false) }}',
                        width: '33%'
                    }
                }
            });
            $jtable.jtable('load');
        });
    </script>
@endsection