@extends('layouts.app')
@section('styles')
    <link href="{{ asset('js/libs/jquery-ui/jquery-ui.css') }}" rel="stylesheet">
    <link href="{{ asset('js/libs/jtable/themes/metro/lightgray/jtable.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Imágenes</div>

                    <div class="panel-body">

                            <div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"  class="active"><a href="#plus-kiosko" aria-controls="plus-kiosko" role="tab" data-toggle="tab">PLUS</a></li>
                                    <li role="presentation"><a href="#categorias-kiosko" aria-controls="categorias-kiosko" role="tab" data-toggle="tab">CATEGORÍAS</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="plus-kiosko">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-xs-12"><!-- Filtros para JTable de plus --></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12" id="contenedor-jtable"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="categorias-kiosko">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-xs-12"><!-- Filtros para JTable de Categorias --></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12" id="contenedor-jtable-categorias"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('endbody')
    <div id="modal-foto-plu" title="Foto">
    </div>
    <div id="modal-editar-foto-plu" title="Modificar Foto" style="display:none">
        <form id="form-editar-foto-plu" enctype="multipart/form-data" method="POST" action="{{ route('web_imagenes_guardar_plus',[],false) }}" >
            <div class="form-group">
                <label for="file">Selecciona el archivo (Solo se aceptan imágenes .png)</label>
                <input type="file" id="foto" name="foto" accept="image/x-png" />
                <input type="hidden" name="tipoimagen" />
                <input type="hidden" name="idproducto" />
            </div>
            <div class="input-group">
                <button type="submit" class="btn btn-danger">Guardar</button>
                <button type="reset" id="botonCerrarModaleEditarImagen" class="btn btn-success">Cancelar</button>
            </div>
        </form>
    </div>
@endsection
@section('javascripts')
    <script src="{{ asset('js/libs/jquery-ui/jquery-ui.js') }}"></script>
    <script src="{{ asset('js/libs/jtable/jquery.jtable.js') }}"></script>
    <script type="text/javascript">
        var $formularioEdicionFotos=$("#form-editar-foto-plu");

        var $modalFotos=$("#modal-foto-plu");
        var $modalEditarFotos=$("#modal-editar-foto-plu");

        var $inputIdProducto=$("input[name='idproducto']");
        var $inputTipoImagen=$("input[name='tipoimagen']");

        var $jtable=$('#contenedor-jtable');
        var $jtableMenus=$('#contenedor-jtable-categorias');

        var $botonCerrarModalEditarImagen=$("#botonCerrarModaleEditarImagen");

        (function($) {
            $.fn.serializeFiles = function() {
                var obj = $(this);
                /* ADD FILE TO PARAM AJAX */
                var formData = new FormData();
                $.each($(obj).find("input[type='file']"), function(i, tag) {
                    $.each($(tag)[0].files, function(i, file) {
                        formData.append(tag.name, file);
                    });
                });
                var params = $(obj).serializeArray();
                $.each(params, function (i, val) {
                    formData.append(val.name, val.value);
                });
                return formData;
            };
        })(jQuery);


        $modalFotos.dialog({
            autoOpen:false,
            position:{ my: "center", at: "center", of: window },
            width:350,
            show: { effect: "fade", duration: 500 },
            hide: { effect: "fade", duration: 500 }
        });


        $modalEditarFotos.dialog({
            autoOpen:false,
            position:{ my: "center", at: "center", of: window },
            width:350,
            show: { effect: "fade", duration: 500 },
            hide: { effect: "fade", duration: 500 }
        });

        var mostrarModalVerFoto = function (evt) {
            var record=evt.data.record;
            var urlFoto=record.foto;

            var img = $("<img width=300 />").attr('src', urlFoto)
                .on('load', function() {
                    if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
                        $modalFotos.html("<h2>No se encontró la imagen</h2>");
                    } else {
                        $modalFotos.html(img);
                    }
                })
                .on('error',function(){
                    $modalFotos.html("<h2>No se encontró la imagen</h2>");
                });
            //$modalFotos.html("<img src='"+record.foto+"' width=300 />");
            $modalFotos.dialog("open");
        };

        var mostrarModalEditarFoto = function(evt){
            var record=evt.data.record;
            var tipo=evt.data.tipo;
            if("producto" === tipo) $inputIdProducto.val(record.identificador);
            if("categoria" === tipo) $inputIdProducto.val(record.identificador);
            $inputTipoImagen.val(tipo);
            $modalEditarFotos.dialog("open");
        };

        $botonCerrarModalEditarImagen.on("click",function(){
            $modalEditarFotos.dialog("close");
            return true;
        });

        $(document).ready(function () {

            $jtableMenus.jtable({
                title: 'Categorías Plus Kiosko',
                actions: {
                    listAction: '{{ route('web_imagenes_listar_categorias',[],false) }}',
                },
                fields: {
                    identificador: {
                        key: true,
                        list: false,
                        title: 'ID',
                        width: '15%'
                    },
                    nombre: {
                        title: 'Nombre',
                        width: '33%'
                    },
                    foto: {
                        title: 'Ver Foto',
                        width: '5%',
                        display: function(data) {
                            var $btn=$("<button class='btn btn-info'/>");
                            $btn.html("<i class='glyphicon glyphicon-large glyphicon-eye-open'></i>");
                            $btn.on("click",{record:data.record},mostrarModalVerFoto);
                            return $btn;
                        }
                    },
                    EditarFoto: {
                        title: 'Editar Foto',
                        width: '5%',
                        display: function(data) {
                            var $btn=$("<button class='btn btn-info' />");
                            $btn.html("<i class='glyphicon glyphicon-large glyphicon-edit'></i>");
                            $btn.on("click",{
                                record:data.record,
                                tipo:"categoria"
                            },mostrarModalEditarFoto);
                            return $btn;
                        }
                    }
                }
            });
            $jtableMenus.jtable('load');

            $jtable.jtable({
                title: 'Productos Kiosko',
                actions: {
                    listAction: '{{ route('web_imagenes_listar_plus',[],false) }}',
                },
                fields: {
                    identificador: {
                        key: true,
                        list: true,
                        title: 'ID',
                    },
                    nombre: {
                        title: 'Nombre',
                        width: '33%'
                    },
                    foto: {
                        title: 'Ver Foto',
                        width: '5%',
                        display: function(data) {
                            var $btn=$("<button class='btn btn-info'/>");
                            $btn.html("<i class='glyphicon glyphicon-large glyphicon-eye-open'></i>");
                            $btn.on("click",{record:data.record},mostrarModalVerFoto);
                            return $btn;
                        }
                    },
                    EditarFoto: {
                        title: 'Editar Foto',
                        width: '5%',
                        display: function(data) {
                            var $btn=$("<button class='btn btn-info' />");
                            $btn.html("<i class='glyphicon glyphicon-large glyphicon-edit'></i>");
                            $btn.on("click",
                                {
                                    record:data.record,
                                    tipo:"producto"
                                },
                                mostrarModalEditarFoto);
                            return $btn;
                        }
                    }
                }
            });
            $jtable.jtable('load');


            $formularioEdicionFotos.submit(function(e) {
                event.preventDefault();
                $form = $(this);
                var filesdata = new FormData(this);
                $.ajax({
                    url: $form.attr("action"),
                    data: $form.serializeFiles(),
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST', // For jQuery < 1.9
                    success: function(data){
                        $modalEditarFotos.dialog("close");
                        if("producto" ===$inputTipoImagen.val()) $jtable.jtable('reload');
                        if("categoria" === $inputTipoImagen.val()) $jtableMenus.jtable('reload');
                    }
                });
                return false;

            });
        });

    </script>
@endsection
