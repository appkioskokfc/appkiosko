## Kiosko KFC

Aplicación creada utilizando el framework Laravel de PHP para generar los servicios web necesarios
para la integración de los Kiosko con el sistema Maxpoint de KFC.

Algunas características del framework son las siguientes (En Inglés):

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

##Instalación

## Licencias
    Laravel
    The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
