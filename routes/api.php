<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('logout', 'Auth\LoginController@logout')->name("api_logout");

    Route::get('menus', 'Api\MenuController@menus')->name("api_menus");

    Route::get('preguntas/{pluid}', 'Api\MenuController@preguntasSugeridas')
        ->where('pluid', '[0-9]+')
        ->name("api_preguntas_sugeridas");

    Route::get('formaspago', 'Api\FormasPagoController@formasPagoAppKiosko')->name("api_formas_pago");

    Route::get('tiposdocumento','Api\TipoDocumentoController@listar')->name("api_tipos_documento");

    Route::post('cliente/validar', 'Api\ClienteController@validarCliente')->name('api_validar_cliente');

    Route::get('cliente/{documento}', 'Api\ClienteController@buscar')->name('api_buscar_cliente');

    Route::post('pedido/montosparciales', 'Api\PedidoController@montosParciales')->name('api_montos_parciales_pedido');

    Route::resource('pedidos','Api\PedidoController');

    Route::post('iniciarventas', 'Api\LoginController@iniciarControlEstacion')->name('api_iniciar_ventas');

    Route::post('finalizarventas', 'Api\LoginController@finalizarControlEstacion')->name('api_finalizar_ventas');

    Route::get('validaradministrador/{cedula}', 'Api\LoginController@validarCedulaAdministrador')->name('api_validar_administrador');

	Route::get('kiosk/status', 'Api\LoginController@estadoAsignacionKiosko')->name('api_estado_asignacion_kiosko');

	Route::get('reportes/ventas-switch', 'Api\ReportesController@ventasSwitchKiosko')->name('api_reportes_ventas_switch');

    Route::get('imagenes/promociones', 'Api\ImagenesController@promociones')->name('api_imagenes_promociones');
    Route::get('imagenes/historias', 'Api\ImagenesController@historias')->name('api_imagenes_historias');
    Route::get('imagenes/pantallainicio', 'Api\ImagenesController@pantallaInicio')->name('api_imagenes_pantalla_inicio');

});

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');

Route::post('imagenes/sincronizar/pantallainicio','Api\ImagenesController@sincronizarAzureImagenesPantallaInicio')->name('api_imagenes_sincronizar_pantalla_inicio');