<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function(){
    Route::get('admin/estaciones', ['as' => 'web_estaciones_listar', 'uses' => 'Admin\EstacionesController@listar']);
    Route::post('admin/estaciones', ['as' => 'web_estaciones_listar_ajax', 'uses' => 'Admin\EstacionesController@listar']);
    Route::post('admin/estaciones/crear', ['as' => 'web_estaciones_crear', 'uses' => 'Admin\EstacionesController@crear']);
    Route::post('admin/estaciones/editar', ['as' => 'web_estaciones_editar', 'uses' => 'Admin\EstacionesController@editar']);
    Route::post('admin/estaciones/eliminar', ['as' => 'web_estaciones_eliminar', 'uses' => 'Admin\EstacionesController@eliminar']);

    Route::get('admin/imagenes', ['as' => 'web_imagenes_listar', 'uses' => 'Admin\ImagenesController@listar']);
    Route::post('admin/imagenes/listarplus', ['as' => 'web_imagenes_listar_plus', 'uses' => 'Admin\ImagenesController@listarPlus']);
    Route::post('admin/imagenes/listarcategorias', ['as' => 'web_imagenes_listar_categorias', 'uses' => 'Admin\ImagenesController@listarCategorias']);

    Route::post('admin/imagenes/guardar', ['as' => 'web_imagenes_guardar_plus', 'uses' => 'Admin\ImagenesController@guardarImagen']);

    Route::post('admin/estaciones/estacionesmaxpoint', ['as' => 'web_estaciones_maxpoint', 'uses' => 'Admin\EstacionesController@comboEstacionesKioskoMaxpoint']);
    Route::post('admin/estaciones/usuariosmaxpoint', ['as' => 'web_usuarios_maxpoint', 'uses' => 'Admin\EstacionesController@comboUsuariosKioskoMaxpoint']);
});

//Rutas de proceso de registro,login y logout
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

