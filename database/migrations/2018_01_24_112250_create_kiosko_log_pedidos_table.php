<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKioskoLogPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kiosko_log_pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('json_texto');
            $table->text('mensaje_error')->nullable();
            $table->string('ip',46);
            $table->enum('estado', ['recibido','error', 'procesado'])->default('recibido');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kiosko_log_pedidos');
    }
}
