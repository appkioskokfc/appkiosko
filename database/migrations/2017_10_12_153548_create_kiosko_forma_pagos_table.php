<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKioskoFormaPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     *
     * @return void
     * */
    public function up()
    {
        Schema::create('kiosko_forma_pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idOrden');
            $table->string('bin',6);
            $table->decimal('fpf_total_pagar',10,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kiosko_forma_pagos');
    }
}
