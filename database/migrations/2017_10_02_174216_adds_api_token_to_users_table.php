<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddsApiTokenToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kiosko_users', function (Blueprint $table) {
            $table->string('api_token', 400)->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kiosko_users', function (Blueprint $table) {
            $table->dropUnique('users_api_token_unique');
            $table->dropUnique('users_email_unique');
            $table->dropColumn(['api_token']);
        });
    }
}
