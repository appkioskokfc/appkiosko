<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKioskoCabeceraPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kiosko_cabecera_pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cli_nombres',100);
            $table->string('IDTipoDocumento');
            $table->string('cli_documento');
            $table->string('cli_telefono',11)->nullable();
            $table->string('cli_direccion',200)->nullable();
            $table->string('cli_email',50)->nullable();
            $table->decimal('cfac_subtotal',10,2);
            $table->decimal('cfac_iva',10,2);
            $table->decimal('cfac_total',10,2);
            $table->enum('tipo_servicio', ['salon', 'llevar'])->nullable();
            $table->string('cfac_id',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kiosko_cabecera_pedidos');
    }
}
