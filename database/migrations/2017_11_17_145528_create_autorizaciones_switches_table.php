<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutorizacionesSwitchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kiosko_autorizaciones_switch', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idOrden');
            $table->string('codigoComercio');
            $table->string('numeroTerminal');
            $table->string('lote');
            $table->string('referencia');
            $table->string('autorizacion');
            $table->string('tarjetaHabiente');
            $table->string('numeroTarjeta');
            $table->string('numAdquiriente');
            $table->string('codigoResultado');
            $table->string('mensajeResultado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kiosko_autorizaciones_switch');
    }
}
