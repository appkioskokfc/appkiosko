<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKioskoDetallePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kiosko_detalle_pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_orden');
            $table->integer('plu_id');
            $table->integer('dop_cantidad');
            $table->integer('modifica')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kiosko_detalle_pedidos');
    }
}
