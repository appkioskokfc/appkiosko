<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Usuario para consumir la API desde los kioskos
        DB::table('kiosko_users')->insert([
            'name' => 'kiosko',
            'email' => 'kiosko@kfc.com',
            'password' => bcrypt('appkiosko'),
        ]);

        //Usuario para consumir los reportes
        DB::table('kiosko_users')->insert([
            'name' => 'reporteskiosko',
            'email' => 'reporteskiosko@kfc.com',
            'password' => bcrypt('r3porT3s.KFC'),
        ]);
    }
}
